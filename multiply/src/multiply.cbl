IDENTIFICATION DIVISION.
AUTHOR. kevinwojo.
PROGRAM-ID. funtimes.

DATA DIVISION.
WORKING-STORAGE SECTION.
01 A PIC 9(5).
01 B PIC 9(5).
01 C PIC 9(5).

PROCEDURE DIVISION.
    DISPLAY "Enter a number: ".
    ACCEPT A.
    DISPLAY "Enter another number: ".
    ACCEPT B.
    COMPUTE C = A * B.
    DISPLAY "Result = ", C.
    IF C < 100 THEN
      DISPLAY "Result is less than 100"
    ELSE
      DISPLAY "Result is greater than 100"
    END-IF
    STOP RUN.
    
